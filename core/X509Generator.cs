using System;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace core
{
    public class X509Generator : ICertGenerator
    {
        private const int CaYears = 5;
        private const int CertYears = 1;
        
        public X509Certificate2 GenerateCA(string cn)
        {
            var key = RSA.Create();
            var req = new CertificateRequest($"cn={cn}", key, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
            req.CertificateExtensions.Add(
                new X509BasicConstraintsExtension(true, false, 0, true));
            req.CertificateExtensions.Add(
                new X509SubjectKeyIdentifierExtension(req.PublicKey, false));
            var cert = req.CreateSelfSigned(DateTimeOffset.Now, DateTimeOffset.Now.AddYears(CaYears));
            
            X509KeyStorageFlags storageFlags = X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable;
            return new X509Certificate2(
                cert.Export(X509ContentType.Pkcs12, ""),
                "",
                storageFlags);
        }

        public X509Certificate2 GenerateSigned(string cn, X509Certificate2 ca)
        {
            var key = RSA.Create();
            var serialNumber = GenerateSerialNumber();
            
            SubjectAlternativeNameBuilder sanBuilder = new SubjectAlternativeNameBuilder();
            sanBuilder.AddIpAddress(IPAddress.Loopback);
            sanBuilder.AddIpAddress(IPAddress.IPv6Loopback);
            sanBuilder.AddDnsName("localhost");
            sanBuilder.AddDnsName(Environment.MachineName);
            sanBuilder.AddIpAddress(IPAddress.Parse(cn));
            
            var req = new CertificateRequest($"cn={cn}", key, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
            req.CertificateExtensions.Add(
                new X509KeyUsageExtension(
                            X509KeyUsageFlags.KeyEncipherment 
                            | X509KeyUsageFlags.DigitalSignature,
                      false)
                );
           
            // 1.3.6.1.5.5.7.3.1
            // 1.3.6.1.5.5.7.3.1 = Transport Layer Security (TLS) World Wide Web (WWW) server authentication
            // var oid = Oid.FromFriendlyName("TLS Web Server Authentication", OidGroup.EnhancedKeyUsage);
            // var oid = Oid.FromFriendlyName("Server Authentication", OidGroup.EnhancedKeyUsage);
            // var oid = Oid.FromOidValue("1.3.6.1.5.5.7.3.1", OidGroup.EnhancedKeyUsage);
            var oid = new Oid("1.3.6.1.5.5.7.3.1");
            var oids = new OidCollection(){ oid };
            req.CertificateExtensions.Add(new X509EnhancedKeyUsageExtension(oids, true));
            req.CertificateExtensions.Add(sanBuilder.Build());
            var cert = req.Create(ca, DateTimeOffset.Now, DateTimeOffset.Now.AddYears(CertYears), serialNumber);
            cert = cert.CopyWithPrivateKey(key);

            X509KeyStorageFlags storageFlags = X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable;
            return new X509Certificate2(
                cert.Export(X509ContentType.Pkcs12, ""),
                "",
                storageFlags);
        }

        private static byte[] GenerateSerialNumber()
        {
            byte[] serialNumber = new byte[8];
            RandomNumberGenerator.Fill(serialNumber);
            return serialNumber;
        }
    }
}