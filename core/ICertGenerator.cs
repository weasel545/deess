using System.Security.Cryptography.X509Certificates;

namespace core
{
    public interface ICertGenerator
    {
        public X509Certificate2 GenerateCA(string cn);
        public X509Certificate2 GenerateSigned(string cn, X509Certificate2 ca);
    }
}