﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace core
{
    public class X509CertStore : ICertStore
    {
        private readonly string _caSubjectName;
        private readonly StoreLocation _certStoreLocation;
        private readonly StoreLocation _caStoreLocation;

        public X509CertStore(string caSubjectName)
        {
            _caSubjectName = caSubjectName + "-ca";
            _certStoreLocation = StoreLocation.CurrentUser;
            _caStoreLocation = StoreLocation.CurrentUser;
        }

        public void EnsureCaExists(ICertGenerator generator)
        {
            var ca = CaCert();

            if (ca != null)
            {
                Console.WriteLine("Ca certificate already exists");
                return;
            }

            Console.Write("Creating new ca certificate ... ");
            ca = generator.GenerateCA(_caSubjectName);
            AddToStore(ca, _caStoreLocation);
            Console.WriteLine("DONE.");
        }

        public X509Certificate2 CaCert()
        {
            return FindCertByCN(_caSubjectName, _caStoreLocation);
        }

        private X509Certificate2 FindCertByCN(string cn, StoreLocation storeLocation)
        {
            
            var store = Store(storeLocation);
            store.Open(OpenFlags.ReadOnly);

            X509Certificate2 result = null; 
            
            foreach (X509Certificate2 cert in store.Certificates)
            {
                if ("CN=" + cn == cert.Subject)
                {
                    result = cert;
                    break;
                }
            }
            
            store.Close();
            return result;
        }
        private static void AddToStore(X509Certificate2 cert, StoreLocation storeLocation)
        {
            var store = Store(storeLocation);
            store.Open(OpenFlags.ReadWrite);
            store.Add(cert);
            store.Close();
        }

        private static X509Store Store(StoreLocation location)
        {
            StoreName storeName = StoreName.My;
            X509Store store = new X509Store(storeName, location);
            return store;
        }

        public bool EnsureCaAbsend()
        {
            return RemoveCertIfExists(CaCert(), _caStoreLocation);
        }

        private bool RemoveCertIfExists(X509Certificate2 cert, StoreLocation location)
        {
            if (cert != null)
            {
                var store = Store(location);
                store.Open(OpenFlags.ReadWrite);
                store.Remove(cert);
                store.Close();
                return true;
            }

            return false;
        }

        public void EnsureCertExists(ICertGenerator generator, string cn)
        {
            var cert = Cert(cn);
            if (cert != null)
            {
                Console.WriteLine($"Certificate for {cn} already exists");
                return;
            }

            var caCert = CaCert();
            if (caCert == null)
            {
                throw new NullReferenceException("CA Certificate is null; Call EnsureCaExists() first;");
            }
            
            Console.Write($"Creating new server certificate for {cn} ... ");
            cert = generator.GenerateSigned(cn, caCert);
            AddToStore(cert, _certStoreLocation);
            Console.WriteLine("DONE.");
        }


        public bool EnsureCertAbsend(string cn)
        {
            var result = RemoveCertIfExists(Cert(cn), _certStoreLocation);
            Console.WriteLine($"Certificate for {cn} removed");

            return result;
        }

        public void EnsureCertAbsend(X509Certificate2 cert)
        {
            RemoveCertIfExists(cert, _certStoreLocation);
        }

        public X509Certificate2 Cert(string cn)
        {
            return FindCertByCN(cn, _certStoreLocation);
        }

        public List<X509Certificate2> AllSignedCerts()
        {
            var result = new List<X509Certificate2>(); 
            var ca = CaCert();

            if (ca == null)
            {
                return result;
            }
            
            var store = Store(_certStoreLocation);
            
            store.Open(OpenFlags.ReadOnly);
            foreach (X509Certificate2 cert in store.Certificates)
            {
                if (cert.SerialNumber != null && cert.SerialNumber.Equals(ca.SerialNumber))
                {
                    // ignore ca cert its self
                    continue;
                }
                
                X509Chain verify = new X509Chain();
                verify.ChainPolicy.ExtraStore.Add(ca);
                verify.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
                verify.ChainPolicy.RevocationMode = X509RevocationMode.NoCheck;
                
                if (verify.Build(cert))
                {
                    foreach (var cur in verify.ChainElements)
                    {
                        if (cur.Certificate.Thumbprint == ca.Thumbprint)
                        {
                            result.Add(cert);
                            break;
                        }
                    }
                }
            }
            store.Close();
            return result;
        }
    }
}