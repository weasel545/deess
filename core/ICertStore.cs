using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace core
{
    public interface ICertStore
    {
        void EnsureCaExists(ICertGenerator generator);
        void EnsureCertExists(ICertGenerator generator, string cn);
        X509Certificate2 CaCert();
        bool EnsureCaAbsend();
        bool EnsureCertAbsend(string cn);
        public X509Certificate2 Cert(string cn);
        List<X509Certificate2> AllSignedCerts();
    }

}