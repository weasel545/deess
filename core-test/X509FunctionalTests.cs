using System.Security.Cryptography.X509Certificates;
using core;
using NUnit.Framework;

namespace cli_cert_test
{
    public class X509FunctionalTests
    {
        private ICertStore _sut;
        private ICertGenerator _generator;
        private const string SampleIp = "192.168.10.241";

        [SetUp]
        public void Setup()
        {
            _sut = new X509CertStore("deess-unit-testing");
            _generator = new X509Generator();
        }

        [TearDown]
        public void TearDown()
        {
            while (_sut.EnsureCertAbsend("foo"))
            {
                // emtpy by intention
            }
            while (_sut.EnsureCertAbsend("bar"))
            {
                // emtpy by intention
            }
            while (_sut.EnsureCertAbsend(SampleIp))
            {
                // emtpy by intention
            }
            while (_sut.EnsureCaAbsend())
            {
                // emtpy by intention
            }
        }
        
        [Test]
        public void EnsureCaExists_Always_CallsGenerateOnOnlyOnce()
        {
            Assert.Null(_sut.CaCert());
            
            _sut.EnsureCaExists(_generator);
            
            var actual = _sut.CaCert();
            Assert.NotNull(actual);
            Assert.AreEqual("CN=deess-unit-testing-ca", actual.Subject);
            Assert.IsTrue(actual.HasPrivateKey);
        }
        
        [Test]
        public void EnsureCertExists_Always_CallsGenerateOnOnlyOnce()
        {
            Assert.Null(_sut.Cert(SampleIp));
            
            _sut.EnsureCaExists(_generator);
            _sut.EnsureCertExists(_generator, SampleIp);
            
            var actual = _sut.Cert(SampleIp);
            Assert.NotNull(actual);
            Assert.AreEqual("CN=192.168.10.241", actual.Subject);
            Assert.IsTrue(actual.HasPrivateKey);
        }

        [Test]
        public void AllSignedCerts_Always_ReturnsAllSignedCerts()
        {
            _sut.EnsureCaExists(_generator);
            _sut.EnsureCertExists(_generator, "foo");
            _sut.EnsureCertExists(_generator, "bar");

            var actual = _sut.AllSignedCerts();

            Assert.Contains(_sut.Cert("foo"), actual);
            Assert.Contains(_sut.Cert("bar"), actual);
            Assert.AreEqual(2, actual.Count);
        }
    }
}