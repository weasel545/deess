![logo](deess.png)
# dees-proxy

Simple ssl reverse proxy for development and debugging.

It allows to debug APIs/Backends which are called from a mobile app
which was build for production.

## requirements

- dot.net 3.1.403
- valid dev-cert for https
  - debian: https://yob.id.au/2013/02/06/trusting-new-ssl-certificates-in-debian.html
  
## install ca-cert on ios

Instructions

  - Visit http://<you-ip>:7000 in your safari
  - Click download and save the file
  - Open the cert and you will get an option to Install.
  - Click the Install button and you will get a warning message. Proceed further to click install.
  - Now you can see the cert has been installed and you will see  "Done".
  - Make another change under Settings > General > About > Certificate Trust Settings, and there is a section called "ENABLE FULL TRUST FOR ROOT CERTIFICATES".
  - Under it lists the certificate that you have installed on the device. Once enabled, you are done.


## idea

### V2 for 1.0.0
- (/) proxy: ensure ca cert in cert store of localuser 
- (/) proxy: ensure cert for given public ip in cert store of localuser 
- (/) proxy: startup with for given public ip
- (/) proxy: startup serve page with download of certchain the mobile device can trust
- (/) proxy: shudown remove all ca signed certificates
- proxy: fix: if ca hostname changes and ip certs not removed after shutdown - wrong certs will be served
  workaround: delete my keystores (unix /home/{user}/.dotnet/corefx/cryptography/x509stores/my
 
### bugs

- keys not cleaned up correctly, need to remove ca, but this leafs certs back
 
### next steps
 
- automatically run on all available ip addresses