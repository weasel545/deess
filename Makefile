DOTNET_CMD=dotnet
PROXY_PRJ_FILE=proxy/proxy.csproj
VERSION=1.0.4

package: build dist dist/deess-$(VERSION)-lin.zip dist/deess-$(VERSION)-win.zip dist/deess-$(VERSION)-mac.zip
re-package: clean package

build: build/lin build/mac build/win
build/lin: build/lin/proxy build/lin/README.md build/lin/deess.png
build/mac: build/mac/proxy build/mac/README.md build/mac/deess.png
build/win: build/win/proxy build/win/README.md build/win/deess.png

build/lin/proxy:
	$(DOTNET_CMD) publish \
		-p:PublishSingleFile=true \
		-p:IncludeSymbolsInSingleFile=true \
		--output=build/lin/proxy \
		--runtime=linux-x64 \
		--self-contained true \
		$(PROXY_PRJ_FILE)
build/mac/proxy:
	$(DOTNET_CMD) publish \
		-p:PublishSingleFile=true \
		-p:IncludeSymbolsInSingleFile=true \
		--output=build/mac/proxy \
		--runtime=osx.10.14-x64 \
		--self-contained true \
		$(PROXY_PRJ_FILE)
build/win/proxy:
	$(DOTNET_CMD) publish \
		-p:PublishSingleFile=true \
		-p:IncludeSymbolsInSingleFile=true \
		--output=build/win/proxy \
		--runtime=win-x64 \
		--self-contained true \
		$(PROXY_PRJ_FILE)
		
build/lin/README.md:
	@cp -v README.user.md build/lin/README.md
build/lin/deess.png:
	@cp -v deess.png build/lin/deess.png
build/win/README.md:
	@cp -v README.user.md build/win/README.md
build/win/deess.png:
	@cp -v deess.png build/win/deess.png
build/mac/README.md:
	@cp -v README.user.md build/mac/README.md
build/mac/deess.png:
	@cp -v deess.png build/mac/deess.png
	
	
	
dist:
	mkdir -pv dist
	
dist/deess-$(VERSION)-lin.zip:
	zip -r dist/deess-$(VERSION)-lin.zip build/lin
	
dist/deess-$(VERSION)-win.zip:
	zip -r dist/deess-$(VERSION)-win.zip build/win
	
dist/deess-$(VERSION)-mac.zip:
	zip -r dist/deess-$(VERSION)-mac.zip build/mac
	
clean:
	rm -rfv build dist