using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Primitives;
using Microsoft.VisualBasic;

namespace proxy
{
    public class ReverseProxyMiddleware
    {
        private static readonly HttpClientHandler HttpClientHandler = new HttpClientHandler() { AllowAutoRedirect = false };
        private static readonly HttpClient HttpClient = new HttpClient(HttpClientHandler);
        private readonly RequestDelegate _nextMiddleware;
        private readonly int _certsPort;

        public ReverseProxyMiddleware(RequestDelegate nextMiddleware)
        {
            _nextMiddleware = nextMiddleware;
            _certsPort = 7000;
        }

        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Host.Port == _certsPort)
            {
                await _nextMiddleware.Invoke(context);
                return;
            }
            
            var backendUri = new Uri("http://127.0.0.1:8000" + context.Request.Path + context.Request.QueryString);
            var backendRequest = CreateBackendRequest(context, backendUri);
            
            // TODO support redirects /api/v1 to /api/v1
            // TODO catch unavailable backends an return 503 back gateway
            
            using (var backendResponse = await HttpClient.SendAsync(backendRequest))
            {
                context.Response.StatusCode = (int) backendResponse.StatusCode;
                CopyFromBackendResponseHeaders(context, backendResponse);
                await ProcessResponseContent(context, backendResponse);
            }
            
            // do not call next middleware - terminate request processing here
        }

        private HttpRequestMessage CreateBackendRequest(HttpContext context, Uri targetUri)
        {
           var backendRequest = new HttpRequestMessage();
           CopyFromOriginalRequestContentAndHeaders(context, backendRequest);

           backendRequest.RequestUri = targetUri;
           backendRequest.Headers.Host = targetUri.Host;
           backendRequest.Method = GetMethod(context.Request.Method);
           backendRequest.Headers.Add("X-Forwarded-For", context.Connection.RemoteIpAddress.ToString());
            // TODO send proxy header so symfony can generate correct urls
            // TODO send X-Forwarded-For: <real ip of client>
            
           return backendRequest;
        }
        
        private void CopyFromOriginalRequestContentAndHeaders(HttpContext context, HttpRequestMessage backendRequest)
        {
            var requestMethod = context.Request.Method;

            if (!HttpMethods.IsGet(requestMethod) &&
                !HttpMethods.IsHead(requestMethod) &&
                !HttpMethods.IsDelete(requestMethod) &&
                !HttpMethods.IsTrace(requestMethod))
            {
                var streamContent = new StreamContent(context.Request.Body);
                backendRequest.Content = streamContent;
            }

            foreach (var (key, value) in context.Request.Headers)
            {
                if (key == "Authorization")
                {
                    backendRequest.Headers.TryAddWithoutValidation(key, value.ToString());
                    continue;
                }
                
                backendRequest.Content?.Headers.TryAddWithoutValidation(key, value.ToString());
            }
        }

        private HttpMethod GetMethod(string method)
        {
            if (HttpMethods.IsDelete(method)) return HttpMethod.Delete;
            if (HttpMethods.IsGet(method)) return HttpMethod.Get;
            if (HttpMethods.IsHead(method)) return HttpMethod.Head;
            if (HttpMethods.IsOptions(method)) return HttpMethod.Options;
            if (HttpMethods.IsPost(method)) return HttpMethod.Post;
            if (HttpMethods.IsPut(method)) return HttpMethod.Put;
            if (HttpMethods.IsTrace(method)) return HttpMethod.Trace;
            
            return new HttpMethod(method);
        }

        private void CopyFromBackendResponseHeaders(HttpContext context, HttpResponseMessage targetResponseMessage)
        {
            foreach (var (header, values) in targetResponseMessage.Headers)
            {
                context.Response.Headers[header] = values.ToArray();
            }
            
            foreach (var (header, values) in targetResponseMessage.Content.Headers)
            {
                context.Response.Headers[header] = values.ToArray();
            }

            if (context.Response.Headers.ContainsKey("Location"))
            {
                RewriteLocationHeader(context);
            }
            
            context.Response.Headers.Remove("transfer-encoding");
        }

        private static void RewriteLocationHeader(HttpContext context)
        {
            StringValues org;
            bool success = context.Response.Headers.TryGetValue("Location", out org);
            if (success)
            {
                context.Response.Headers.Remove("Location");
                var uri = new Uri(org[0]);
                var build = new UriBuilder(uri);
                build.Scheme = context.Request.Scheme;
                build.Host = context.Request.Host.Host;
                build.Port = context.Request.Host.Port.Value;
                var replaced = build.ToString();
                StringValues replacedStringValues = new StringValues(new[] {replaced});
                context.Response.Headers["Location"] = replacedStringValues;
            }
        }

        private static async Task ProcessResponseContent(HttpContext context, HttpResponseMessage response)
        {
            var content = await response.Content.ReadAsByteArrayAsync();

            if (IsContentTypeOf(response, new[] {"application/json"}))
            {
                var stringContent = Encoding.UTF8.GetString(content);
                var newStringContent = RewriteUrls(context, response, stringContent);

                await context.Response.WriteAsync(Strings.Trim(newStringContent), Encoding.UTF8);
                return;
            }

            await context.Response.Body.WriteAsync(content);
        }

        private static string RewriteUrls(HttpContext context, HttpResponseMessage response, string stringContent)
        {
            var backendUri = response.RequestMessage.RequestUri;
            var request = context.Request;
            var newStringContent = stringContent.Replace(
                $"{backendUri.Scheme.ToString()}:\\/\\/{backendUri.Host.ToString()}",
                $"{request.Scheme.ToString()}:\\/\\/{request.Host.ToString()}"
            );
            return newStringContent;
        }

        private static bool IsContentTypeOf(HttpResponseMessage response, IEnumerable<string> allowed)
        {
            var contentType = response.Content?.Headers?.ContentType;
            if (contentType == null)
            {
                return false;
            }
            
            foreach (string s in allowed)
            {
                if (contentType.ToString() == s)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
