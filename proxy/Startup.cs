using System;
using System.Net;
using System.Text;
using core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace proxy
{
    public class Startup
    {
        private IConfiguration Configuration { get; }
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            IConfigurationSection kestrel = Configuration.GetSection("Kestrel");
            
            // skip this because it seems to override our cert: services.Configure<KestrelServerOptions>(kestrel);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<ReverseProxyMiddleware>();
            
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context => { await context.Response.WriteAsync( "<a href=/deess-ca.crt>Download DEESS CA-Certificate</a>" ); });
                endpoints.MapGet("/deess-ca.crt", async context =>
                {
                    var certStore = new X509CertStore("deess-" + Dns.GetHostName().Trim() );
                    var caCert = certStore.CaCert();
                    string caCertString;

                    using (var cert = caCert)
                    {
                        StringBuilder builder = new StringBuilder();
                        builder.AppendLine("-----BEGIN CERTIFICATE-----");
                        builder.AppendLine(
                            Convert.ToBase64String(cert.RawData, Base64FormattingOptions.InsertLineBreaks));
                        builder.AppendLine("-----END CERTIFICATE-----");

                        caCertString = builder.ToString();
                    }

                    context.Response.ContentType = "application/x-pem-file";
                    await context.Response.WriteAsync(caCertString);
                });
            });
        }
    }
}