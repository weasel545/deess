using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using core;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.CommandLineUtils;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace proxy
{
    public class Program
    {
        private static X509CertStore _store;
        private static X509Generator _generator;

        public static void Main(string[] args)
        {
            var app = ConfigureCliApp(args);
            app.Execute(args);
        }

        private static CommandLineApplication ConfigureCliApp(string[] args)
        {
            CommandLineApplication app = new CommandLineApplication(throwOnUnexpectedArg: false);
            app.HelpOption("-? | -h | --help");
            
            var ipArg = app.Option("-$|-i |--ip <ip>", "the ip deess is listening on", CommandOptionType.SingleValue);
            var backendArg = app.Option("-$|-b |--backend <backend-url>", "the url dees is serving for: i.e. http://127.0.0.1", CommandOptionType.SingleValue);
            var cleanArg = app.Option("-$ | -c |--clean", "removes all deess certificates", CommandOptionType.NoValue);
            // TODO app argument to remove all certs including ca cert and exit
            
            app.OnExecute(() =>
            {
                _store = new X509CertStore("deess-" + Dns.GetHostName().Trim() );
                _generator = new X509Generator();
                
                if (cleanArg.HasValue())
                {
                    CleanupCertificates(true);
                    Console.WriteLine("All certificates including CA removed");
                    return 0;
                }
                
                var ips = new List<string> { };
                if (!ipArg.HasValue())
                {
                    throw new InvalidProgramException("Required option ip is missing");
                }

                if (!backendArg.HasValue())
                {
                    throw new InvalidProgramException("Required option backend is missing");
                }
                
                ips.Add(ipArg.Value());
                CleanupCertificates();
                PrepareCertificates(ips);
                CreateProxyHostBuilder(args, ips, backendArg.Value()).Build().Run();
                CleanupCertificates();
                return 0;
            });
            
            return app;
        }

        private static void PrepareCertificates(List<string> ips)
        {
            _store.EnsureCaExists(_generator);
            foreach (string ip in ips)
            {
                _store.EnsureCertExists(_generator, ip);
            }
        }

        private static void CleanupCertificates(bool withCa = false)
        {
            foreach (X509Certificate2 cert in _store.AllSignedCerts())
            {
                _store.EnsureCertAbsend(cert);
                Console.WriteLine($"Removed cert: {cert.SerialNumber} >> {cert.Subject} issued by {cert.Issuer}");
            }

            if (withCa)
            {
                _store.EnsureCaAbsend();
                Console.WriteLine($"Removed ca cert");
            }
        }

        public static IHostBuilder CreateProxyHostBuilder(string[] args, List<string> ips, string backend)
        {
            var ip = ips.First() ?? throw new ArgumentNullException("must have at least one ip");
            
            return (new HostBuilder())
                .ConfigureLogging(factory =>
                {
                    factory.AddConsole();
                    factory.AddDebug();
                    factory.AddFilter("Console", level => level >= LogLevel.Warning);
                    factory.AddFilter("Debug", level => level >= LogLevel.Information);
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseKestrel(options =>
                    {
                        options.Listen(IPAddress.Loopback, 7000, listenOptions =>
                        {
                            listenOptions.Protocols = HttpProtocols.Http1;
                        });
                        options.Listen(IPAddress.Parse(ip), 7000, listenOptions =>
                        {
                            listenOptions.Protocols = HttpProtocols.Http1;
                        });
                        options.Listen(IPAddress.Loopback, 7001, listenOptions =>
                        {
                            var serverCertificate = _store.Cert(ip);
                            listenOptions.UseHttps(serverCertificate); // <- Configures SSL
                            listenOptions.Protocols = HttpProtocols.Http1;
                        });
                        options.Listen(IPAddress.Parse(ip), 7001, listenOptions =>
                        {
                            var serverCertificate = _store.Cert(ip);
                            listenOptions.UseHttps(serverCertificate); // <- Configures SSL
                            listenOptions.Protocols = HttpProtocols.Http1;
                        });
                    });
                    webBuilder.UseStartup<Startup>();
                });
        }
    }
}