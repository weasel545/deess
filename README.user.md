![logo](deess.png)

# dees-proxy
Simple ssl reverse proxy for development and debugging.

It allows to debug APIs/Backends which are called from a mobile app
which was build for production.

## getting started

Assuming your WLAN ip is: `192.168.10.229`

```
# win
./proxy.exe --ip=192.168.10.229 --backend=http://127.0.0.1:8000

# lin/mac
./proxy --ip=192.168.10.229 --backend=http://127.0.0.1:8000
```
    
## install ca-cert on ios

Instructions

  - Visit http://<you-ip>:7000 in your safari
  - Click download and save the file
  - Open the cert and you will get an option to Install.
  - Click the Install button and you will get a warning message. Proceed further to click install.
  - Now you can see the cert has been installed and you will see  "Done".
  - Make another change under Settings > General > About > Certificate Trust Settings, and there is a section called "ENABLE FULL TRUST FOR ROOT CERTIFICATES".
  - Under it lists the certificate that you have installed on the device. Once enabled, you are done.

## install ca-cert on android

In Android 11, to install a CA certificate, users need to manually:

  - Visit http://<you-ip>:7000 in your browser and download ca certificate
  - Open settings
  - Go to 'Security'
  - Go to 'Encryption & Credentials'
  - Go to 'Install from storage'
  - Select 'CA Certificate' from the list of types available
  - Accept a large scary warning
  - Browse to the certificate file on the device and open it
  - Confirm the certificate install
  - Check if certificate is visible in trusted credentials -> user
    - if not install again

## prepare your android app

Since `Android Nougat` and Apps targeting API Level 24 and above, User trusted CAs are 
ignored by default for apps. To make deess working you need to apply the following
network-security-settings to allow trusting deess generated certificates (which were trusted by user):

```
<network-security-config>  
      <base-config>  
            <trust-anchors>  
                <!-- Trust preinstalled CAs -->  
                <certificates src="system" />  
                <!-- Additionally trust user added CAs -->  
                <certificates src="user" />  
           </trust-anchors>  
      </base-config>  
 </network-security-config>
```

Sources:
- [https://android-developers.googleblog.com/2016/07/changes-to-trusted-certificate.html](https://android-developers.googleblog.com/2016/07/changes-to-trusted-certificate.html)
- [https://developer.android.com/training/articles/security-config](https://developer.android.com/training/articles/security-config)

## v1.0.4

  - fix: Authorization header is not send to backend
  - fix: Query string is not send to backend

## v1.0.3

  - fix: certificate could not be created on mac osx
  - fix: oid not found on mac osx

## v1.0.2
 
  - fix: ca misses private key on win hosts
  - fix: win and android do not accept server certificate (connection closed) using now RSA
  - fix: server certificates not removed if time invalid
  - feature: --clean removes all existing certificates including ca
   
## v1.0.1

  - fix: https for cert service not available -> use http now
  
## v1.0.0

### changelog
  - feature: proxy all request to given ip to http://127.0.0.1:8000
    ```
    # win
    ./proxy.exe --ip=192.168.10.229 --backend=http://127.0.0.1:8000
    
    # lin/mac
    ./proxy --ip=192.168.10.229 --backend=http://127.0.0.1:8000
    ```
    
    If your ip is `192.168.10.229`: proxy is served via
    https://192.168.10.229:7001 
    
  - feature: serves ca certificate via http 
    
    If your ip is `192.168.10.229`: ca-cert can be downloaded from
    http://192.168.10.229:7000
  
### known issues
  - `--backend` option is required but takes no effect
  - If hostname changes and ip certs not removed after shutdown - wrong certs will be served
    workaround: delete my keystores (unix /home/{user}/.dotnet/corefx/cryptography/x509stores/my
  - mac only after first lunch:
    ```
    Unhandled exception. System.Security.Cryptography.CryptographicException: Removing the requested certificate would modify user trust settings, and has been denied.
      at Interop.AppleCrypto.X509StoreRemoveCertificate(SafeKeychainItemHandle certHandle, SafeKeychainHandle keychain, Boolean isReadOnlyMode)
      at Internal.Cryptography.Pal.StorePal.AppleKeychainStore.Remove(ICertificatePal cert)
      at System.Security.Cryptography.X509Certificates.X509Store.Remove(X509Certificate2 certificate)
      at core.X509CertStore.RemoveCertIfExists(X509Certificate2 cert, StoreLocation location) in /home/mm/git/caresocial/deess/core/X509CertStore.cs:line 85
      at core.X509CertStore.EnsureCertAbsend(X509Certificate2 cert) in /home/mm/git/caresocial/deess/core/X509CertStore.cs:line 118
      at proxy.Program.CleanupCertificates(Boolean withCa) in /home/mm/git/caresocial/deess/proxy/Program.cs:line 74
      at proxy.Program.<>c__DisplayClass3_0.<ConfigureCliApp>b__0() in /home/mm/git/caresocial/deess/proxy/Program.cs:line 51
      at Microsoft.Extensions.CommandLineUtils.CommandLineApplication.Execute(String[] args)
      at proxy.Program.Main(String[] args) in /home/mm/git/caresocial/deess/proxy/Program.cs:line 23
    ```
    
    workaround: sudo proxy ....

